Order food

Narrative:
In order to get food that i want
As a customer
I want to order food
					 
Scenario:  Choose food
Given a list of restaurant : McDonalds, Richeese, Xing Fu Tang
And the menu : BigMac, Ayam Hitam, Brown Sugar Boba
And the price : 20000, 23000, 25000
When i choose BigMac
Then the order details like restaurant's name : McDonalds and menu's name : BigMac and price : 20000 and quantity : 1 added to cart

Scenario: Add quantity of food
Given an order details in cart like restaurant : McDonalds and menu : BigMac and price : 20000 and quantity : 1
When i add 1 more the BigMac
Then the quantity of BigMac will be change into 2	

Scenario: Reduce quantitiy of food 
Given an order details in cart restaurant :  McDonalds and menu : BigMac and price : 20000 and quantity : 2
When i reduce 1 quantity of the BigMac
Then BigMac quantity will be changed into 1

Scenario: Remove order
Given an order details in cart : McDonalds and menu : BigMac and price : 20000 and quantity : 0
When i remove BigMac
Then my cart is empty

Scenario: add notes for address
Given a blank address notes
When I write my details address : sarjad
Then address notes contain sarjad

Scenario:  add notes product
Given a blank product notes
When I write details request : gak pake cuka mang
Then product notes contain gak pake cuka mang

Scenario: Cancel order
Given an order : McDonals and menu: BigMac and price: 20000 and quantity: 1
When i cancel order
Then the order details : restaurant's name : McDonals and menu's name : Bigmac and price : 20000 and quantity : 1 added to cart