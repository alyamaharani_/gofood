package com.grabg.grabgo.jbehave.steps;
import org.jbehave.core.annotations.*;
import java.util.logging.Logger;

import com.grabg.grabgo.service.*;
import com.grabg.grabgo.model.*;
import java.util.ArrayList;

import static org.junit.Assert.assertSame;
import static org.fest.assertions.Assertions.assertThat;


public class orderFood {
	private ArrayList<Resto> resto;
	private ArrayList<cart> myCart;
	private ArrayList<cart> productDetails;
	private ArrayList<cart> productInCart;
	private ArrayList<cart> emptyCart;
	private orderDetails OrderDetails;
	private ArrayList<cart> givenCart;
	private String addressNotes;
	private String productNotes;
	private ArrayList<cart> orderMenu;
	private ArrayList<cart> myCartBack;
	private ArrayList<cart> givenCartBack;

	
	@Given("a list of restaurant : $givenResto")
	public void givenListOfResto(ArrayList<String> givenResto){
		resto = new ArrayList<Resto>();
		for (String e : givenResto){
			resto.add(new Resto(e));
		}
	}
	
	@Given("the menu : $givenMenu")
	public void givenListOfMenu(ArrayList<String> givenMenu) {
		int i=0;
		for(Resto e:resto) {
			e.setMenu(givenMenu.get(i));
			i++;
		}
	}
	
	@Given("the price : $givenPrice")
	public void givenListOfPrice(ArrayList<Integer> givenPrice) {
		int i=0;
		for(Resto e:resto) {
			e.setPrice(givenPrice.get(i));
			i++;
		}
	}
	
	@When("i choose $menu")
	public void whenIChooseMenu(String menu) {
		OrderDetails = new orderDetails(resto, menu);
		myCart = OrderDetails.getOrderDetails();
	}
	
	@Then("the order details like restaurant's name : $restoProduct and menu's name : $menuProduct and price : $priceProduct and quantity : $quantityProduct added to cart")
	public void menuAdded(String restoProduct, String menuProduct, int priceProduct, int quantityProduct) {
		givenCart = new ArrayList<cart>();
		givenCart.add(new cart(restoProduct,menuProduct,priceProduct,quantityProduct));
		int i=0;
		for (int j = 0; j < givenCart.size(); j++) {
			assertThat(myCart.get(i).getMenu()).isEqualTo(givenCart.get(i).getMenu());
			assertThat(myCart.get(i).getPrice()).isEqualTo(givenCart.get(i).getPrice());
			assertThat(myCart.get(i).getQuantity()).isEqualTo(givenCart.get(i).getQuantity());
			assertThat(myCart.get(i).getRestoName()).isEqualTo(givenCart.get(i).getRestoName());
			i++;
		}
	}
	
	@Given("an order details in cart like restaurant : $restoProduct and menu : $menuProduct and price : $priceProduct and quantity : $quantityProduct")
	public void detailsInCart(String restoProduct, String menuProduct, int priceProduct, int quantityProduct){
		productDetails = new ArrayList<cart>();
		productDetails.add(new cart(restoProduct,menuProduct,priceProduct,quantityProduct));
	}
	
	@When("i add $quantity more the $food")
	public void addQuantity(int quantity,String food){
		for (cart e : productDetails){
			if(e.getMenu().equals(food)) {
				e.setQuantity(quantity);
			}
		}
	}
	
	@Then("the quantity of $food will be change into $newQuantity")
	public void quantityChange(String food, int newQuantity) {
		for (cart e : productDetails){
			if(e.getMenu().equals(food)) {
				assertSame(newQuantity,e.getQuantity());
			}
		}
	}
	
	@Given("an order details in cart restaurant :  $restoProduct and menu : $menuProduct and price : $priceProduct and quantity : $quantityProduct")
	public void detailOrdersInCart(String restoProduct, String menuProduct, int priceProduct, int quantityProduct){
		productInCart = new ArrayList<cart>();
		productInCart.add(new cart(restoProduct,menuProduct,priceProduct,quantityProduct));
	}
	@When("i reduce $quantity quantity of the $food")
	public void reduceQuantityOfFood(int quantity, String food){
		for (cart e : productInCart){
			if(e.getMenu().equals(food)) {
				e.setQuantity(-1*quantity);
			}
		}
	}
	@Then("$food quantity will be changed into $newQuantity")
	public void quantityFoodChangedInCart(String food, int newQuantity) {
		for (cart e : productInCart){
			if(e.getMenu().equals(food)) {
				assertSame(newQuantity,e.getQuantity());
			}
		}
	}
	
	@Given("an order details in cart : $restoProduct and menu : $menuProduct and price : $priceProduct and quantity : $quantityProduct")
	public void givenCartIsNull(String restoProduct, String menuProduct, int priceProduct, int quantityProduct){
		emptyCart = new ArrayList<cart>();
		emptyCart.add(new cart(restoProduct,menuProduct,priceProduct,0));
	}

	@When("i remove $menu")
	public void whenIRemoveMenu(String menu) {
		emptyCart.remove(0);
	}
	
	@Then("my $cart is empty")
	public void cartisNull() {
		assertSame(emptyCart.size(),0);
	}
	
	@Given("a blank address notes")
	public void givenBlankAddressNotes(){
		addressNotes = "";
		}
	
	@When("I write my details address : $address")
	public void whenIWriteAddress(String address) {
		addressNotes = address;
	}
	
	@Then("address notes contain $address")
	public void thenNotesContainAddress(String address) {
		assertThat(addressNotes).isEqualTo(address);
	}

	@Given("a blank product notes")
	public void givenBlankProductNotes(){
		productNotes = "";
	}
			
	@When("I write details request : $Notes")
	public void whenIWriteProductNotes(String Notes) {
		productNotes = Notes;
	}
	
	@Then("product notes contain $Notes")
	public void thenNotesContrain(String Notes) {
		assertThat(productNotes).isEqualTo(Notes);
	}
	@Given("an order : $restoProduct and menu: $menuProduct and price: $priceProduct and quantity: $quantityProduct")
	public void givenOrder(String restoProduct, String menuProduct, int priceProduct, int quantityProduct) {
		orderMenu = new ArrayList<cart>();
		orderMenu.add(new cart(restoProduct,menuProduct,priceProduct,quantityProduct));
	}
	
	@When("i cancel order")
	public void whenICancelOrder() {
		myCartBack=new ArrayList<cart>();
		myCartBack=orderMenu;
//		orderMenu.remove(0);
	}
	
	@Then("the order details : restaurant's name : $restoProduct and menu's name : $menuProduct and price : $priceProduct and quantity : $quantityProduct added to cart")
	public void backToTheCart(String restoProduct, String menuProduct, int priceProduct, int quantityProduct) {
		givenCartBack = new ArrayList<cart>();
		givenCartBack.add(new cart(restoProduct,menuProduct,priceProduct,quantityProduct));
		int i=0;
		for (int j = 0; j < givenCart.size(); j++) {
			assertThat(myCartBack.get(i).getMenu()).isEqualTo(givenCartBack.get(i).getMenu());
			assertThat(myCartBack.get(i).getPrice()).isEqualTo(givenCartBack.get(i).getPrice());
			assertThat(myCartBack.get(i).getQuantity()).isEqualTo(givenCartBack.get(i).getQuantity());
			assertThat(myCartBack.get(i).getRestoName()).isEqualTo(givenCartBack.get(i).getRestoName());
			i++;
		}
	}
}