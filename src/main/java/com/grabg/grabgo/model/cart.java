package com.grabg.grabgo.model;

public class cart extends Resto {
	private String restoName;
	private String menu;
	private int price;
	private int quantity;

	public cart(String restoName, String menu, int price, int quantity) {
		super(restoName);
		this.menu = menu;
		this.price = price;
		this.quantity = quantity;
	}
	
	public int getQuantity() {
		return quantity;
	}
	
	public void setQuantity(int quantity) {
		this.quantity += quantity;
	}
}
