package com.grabg.grabgo.model;

public class Resto {
	private String restoName;
	private String menu;
	private int price;

	public Resto(String restoName) {
		this.restoName = restoName;
		this.menu = "";
		this.price = 0;
	}
	
	public String getRestoName() {
		return restoName;
	}
	
	public void setRestoName(String restoName) {
		this.restoName = restoName;
	}
		public String getMenu() {
		return menu;
	}
	
	public void setMenu(String menu) {
		this.menu = menu;
	}
		
	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}
}

