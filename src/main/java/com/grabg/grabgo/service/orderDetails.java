package com.grabg.grabgo.service;
import com.grabg.grabgo.model.*;
import java.util.ArrayList;

public class orderDetails {
	private ArrayList<Resto> restoList;
	private ArrayList<cart> myCart;
	public String menu;
	
	
	
	public orderDetails(ArrayList<Resto> resto, String menu) {
		this.restoList = resto;
		this.menu = menu;
	}
	
	public ArrayList<cart> getOrderDetails(){
		myCart = new ArrayList<cart>();
		for(Resto e : restoList) {
			if(menu.equals(e.getMenu())) {
				myCart.add(new cart(e.getRestoName(),e.getMenu(),e.getPrice(),1));
			}
		}
		return myCart;
	}
}
